from pymongo import MongoClient
from pprint import pprint


client = MongoClient('mongodb://localhost:9876/')


def fetch_max_actions():
    result = client['test']['docs'].aggregate([
        {
            '$unwind': {
                'path': '$acts'
            }
        }, {
            '$unwind': {
                'path': '$acts.scenes'
            }
        }, {
            '$project': {
                '_id': 0,
                'piece': '$_id',
                'act': '$acts.title',
                'scene': '$acts.scenes.title',
                'len': {
                    '$size': '$acts.scenes.action'
                }
            }
        }, {
            '$sort': {
                'len': -1
            }
        }, {
            '$limit': 1
        }
    ])
    if result:
        return tuple(result)[0]


def fetch_not_unique_characters():
    result = client['test']['docs'].aggregate([
        {
            '$unwind': {
                'path': '$acts'
            }
        }, {
            '$unwind': {
                'path': '$acts.scenes'
            }
        }, {
            '$unwind': {
                'path': '$acts.scenes.action'
            }
        }, {
            '$group': {
                '_id': '$acts.scenes.action.character',
                'pieces': {
                    '$addToSet': '$_id'
                }
            }
        }, {
            '$project': {
                '_id': 0,
                'character': '$_id',
                'pieces': '$pieces',
                'count': {
                    '$size': '$pieces'
                }
            }
        }, {
            '$match': {
                'count': {
                    '$gt': 1
                }
            }
        }, {
            '$group': {
                '_id': {},
                'characters': {
                    '$push': '$character'
                }
            }
        }, {
            '$project': {
                '_id': 0
            }
        }
    ])
    if result:
        return tuple(result)[0]


pprint(fetch_max_actions())
pprint(fetch_not_unique_characters())
