```
docker run -p 9876:27017 --rm --name mongo-test -d mongo:latest 
docker exec -i mongo-test sh -c 'mongoimport -c docs -d test --drop' < shakespeare_plays.json
python mongodb_query.py
docker stop mongo-test 
```